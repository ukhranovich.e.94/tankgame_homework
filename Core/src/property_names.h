#pragma once

namespace tank_game::property_names {

const char direction[] = "direction";
const char maxDirection[] = "max_direction";
const char angularVelocity[] = "angular_velocity";

const char position[] = "position";
const char velocity[] = "velocity";

}