#pragma once

#include <vector>

namespace tank_game {

class IMovable
{
public:
	virtual ~IMovable() = default;

	virtual std::vector<int> position() const = 0;
	virtual void setPosition(const std::vector<int>& pos) = 0;

	virtual std::vector<int> velocity() const = 0;
};

}