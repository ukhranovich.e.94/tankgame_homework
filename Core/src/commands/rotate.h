#pragma once

#include "command_interface.h"
#include "rotatable_interface.h"

#include <memory>

namespace tank_game::commands {

class Rotate : public ICommand
{
	std::shared_ptr<IRotatable> _rotatable;

public:
	explicit Rotate(const std::shared_ptr<IRotatable>& rotatable);
	~Rotate() override = default;

	void execute() override;
};

}