#pragma once 

#include "command_interface.h"
#include "movable_interface.h"

#include <memory>

namespace tank_game::commands {

class Move : public ICommand
{
	std::shared_ptr<IMovable> _movable;
public:
	explicit Move(const std::shared_ptr<IMovable>& movable);
	~Move() override = default;

	void execute() override;
};

}