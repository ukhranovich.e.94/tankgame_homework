#include "move.h"

#include "math/vec_ops.h"

#include <stdexcept>

namespace tank_game::commands {

Move::Move(const std::shared_ptr<IMovable>& movable)
	: _movable(movable)
{
	if (nullptr == movable) {
		throw std::invalid_argument("movable must not be null");
	}
}

void Move::execute()
{
	auto position = _movable->position();
	auto velocity = _movable->velocity();

	auto newPosition = math::addVectors(position, velocity);
	_movable->setPosition(newPosition);
}

}