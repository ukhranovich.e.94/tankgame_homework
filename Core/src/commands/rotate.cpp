#include "rotate.h"

#include <stdexcept>

namespace tank_game::commands {

Rotate::Rotate(const std::shared_ptr<IRotatable>& rotatable) 
	: _rotatable(rotatable)
{
	if (nullptr == rotatable) {
		throw std::invalid_argument("rotatable must not be null");
	}
}

void Rotate::execute()
{
	int velocity = _rotatable->angularVelocity();
	int curDirection = _rotatable->direction();
	int maxDirections = _rotatable->maxDirections();

	int newDirection = (curDirection + velocity) % maxDirections;
	_rotatable->setDirection(newDirection);
}

}