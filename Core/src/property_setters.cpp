#include "property_setters.h"

#include "property_names.h"

#include <stdexcept>

namespace tank_game {

void setupMovable(
	IUObject* object,
	const std::vector<int>& pos, 
	const std::vector<int>& velocity)
{
	if (nullptr == object) {
		throw std::invalid_argument("object must no be null");
	}

	object->setProperty(property_names::position, pos);
	object->setProperty(property_names::velocity, velocity);
}

void setupRotatable(IUObject* object, int direction, int maxDirections, int angularVelocity)
{
	if (nullptr == object) {
		throw std::invalid_argument("object must no be null");
	}

	object->setProperty(property_names::direction, direction);
	object->setProperty(property_names::maxDirection, maxDirections);
	object->setProperty(property_names::angularVelocity, angularVelocity);
}

}