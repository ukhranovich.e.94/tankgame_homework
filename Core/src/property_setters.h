#pragma once

#include "uobject_interface.h"

#include <vector>
#include <memory>

namespace tank_game {

void setupMovable(
	IUObject* object, 
	const std::vector<int>& pos,
	const std::vector<int>& velocity
);

void setupRotatable(
	IUObject* object,
	int direction,
	int maxDirections,
	int angularVelocity
);

}