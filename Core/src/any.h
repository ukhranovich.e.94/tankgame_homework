#pragma once

#include <any>
#include <functional>

namespace tank_game {

class Any
{
	std::any _any;
	std::function<bool(const std::any&, const std::any&)> _cmpFunc;

public:
	Any() = default;
	template<class T>
	Any(const T& value)
		: _any(value)
		, _cmpFunc(&Any::compare<T>)
	{ }

	template<class T>
	operator T() const
	{
		return std::any_cast<T>(_any);
	}

	inline bool operator==(const Any& other) const
	{
		if (_any.type() != other._any.type()) {
			return false;
		}

		return _cmpFunc(_any, other._any);
	}

	inline bool operator!=(const Any& other) const
	{
		return !((*this) == other);
	}

private:
	template<class T>
	static bool compare(const std::any& a, const std::any& b)
	{
		const T& valueA = std::any_cast<const T&>(a);
		const T& valueB = std::any_cast<const T&>(b);

		return (valueA == valueB);
	}
};

}