#pragma once

#include <vector>

namespace tank_game::math {

template<class T>
std::vector<T> addVectors(const std::vector<T>& a, const std::vector<T>& b)
{
	auto& biggerVector = (a.size() > b.size())? a : b;
	auto result = biggerVector;
	auto& smallerVector = (a.size() > b.size())? b : a;

	auto resultIter = result.begin();

	for (const T& value : smallerVector) {
		(*resultIter) += value;
		resultIter++;
	}

	return result;
}

}