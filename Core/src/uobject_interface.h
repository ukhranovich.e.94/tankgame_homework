#pragma once

#include "any.h"
#include <string>

namespace tank_game {

class IUObject 
{
public:
	virtual ~IUObject() = default;

	virtual Any property(const std::string& name) const = 0;
	virtual void setProperty(const std::string& name, const Any& value) = 0;
};

}