#include "rotatable.h"

#include "property_names.h"

#include <stdexcept>

namespace tank_game::uobject_adapters {

Rotatable::Rotatable(const std::shared_ptr<IUObject>& object)
	: _object(object)
{
	if (nullptr == object) {
		throw std::invalid_argument("object must not be null");
	}
}

int Rotatable::direction() const
{
	return _object->property(property_names::direction);
}

void Rotatable::setDirection(int direction)
{
	_object->setProperty(property_names::direction, direction);
}

int Rotatable::angularVelocity() const
{
	return _object->property(property_names::angularVelocity);
}

int Rotatable::maxDirections() const
{
	return _object->property(property_names::maxDirection);
}

}