#pragma once

#include "movable_interface.h"
#include "uobject_interface.h"

#include <memory>

namespace tank_game::uobject_adapters {

class Movable : public IMovable
{
	std::shared_ptr<IUObject> _object;

public:
	explicit Movable(const std::shared_ptr<IUObject>& object);
	~Movable() override = default;

	virtual std::vector<int> position() const override;
	virtual void setPosition(const std::vector<int>& pos) override;
	virtual std::vector<int> velocity() const override;
};

}