#include "movable.h"

#include "property_names.h"

#include <stdexcept>

namespace tank_game::uobject_adapters {

Movable::Movable(const std::shared_ptr<IUObject>& object)
	: _object(object)
{
	if (nullptr == object) {
		throw std::invalid_argument("object must not be null");
	}
}

std::vector<int> Movable::position() const
{
	return _object->property(property_names::position);
}

void Movable::setPosition(const std::vector<int>& pos)
{
	_object->setProperty(property_names::position, pos);
}

std::vector<int> Movable::velocity() const
{
	return _object->property(property_names::velocity);
}

}