#pragma once

#include "rotatable_interface.h"
#include "uobject_interface.h"

#include <memory>

namespace tank_game::uobject_adapters {

class Rotatable : public IRotatable
{
	std::shared_ptr<IUObject> _object;

public:
	explicit Rotatable(const std::shared_ptr<IUObject>& object);
	~Rotatable() override = default;

	int direction() const override;
	void setDirection(int direction) override;

	int angularVelocity() const override;
	int maxDirections() const override;
};

}