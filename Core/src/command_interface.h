#pragma once

namespace tank_game {

class ICommand
{
public:
	virtual ~ICommand() = default;

	virtual void execute() = 0;
};

}