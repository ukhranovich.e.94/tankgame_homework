#pragma once

#include "uobject_interface.h"

#include <map>

namespace tank_game {

class UObject : public IUObject
{
	std::map<std::string, Any> _properties;

public:
	~UObject() override = default;

	Any property(const std::string& name) const override;
	void setProperty(const std::string& name, const Any& value) override;
};

}