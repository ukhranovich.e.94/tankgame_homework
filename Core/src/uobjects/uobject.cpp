#include "uobject.h"

#include <stdexcept>

namespace tank_game {

Any UObject::property(const std::string& name) const
{
	auto iter = _properties.find(name);
	if (_properties.cend() != iter) {
		auto& item = *iter;
		return item.second;
	}

	throw std::invalid_argument("Property with name \"" + name + "\" not found");
}

void UObject::setProperty(const std::string& name, const Any& value)
{
	_properties[name] = value;
}

}