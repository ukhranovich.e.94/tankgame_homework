#pragma once

namespace tank_game {

class IRotatable
{
public:
	virtual ~IRotatable() = default;

	virtual int direction() const = 0;
	virtual void setDirection(int direction) = 0;

	virtual int angularVelocity() const = 0;
	virtual int maxDirections() const = 0;
};

}