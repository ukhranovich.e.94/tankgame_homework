#include <uobjects/uobject.h>
#include <property_names.h>
#include <property_setters.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <vector>
#include <stdexcept>

using namespace ::testing;
using namespace tank_game;

TEST(UObjectTest, PorpertyGetSet)
{
	const char intPropName[] = "int_prop";
	const char vecPropName[] = "vec_prop";

	auto uobject = std::make_shared<UObject>();

	const int testIntValue = 4256;
	uobject->setProperty(intPropName, testIntValue);
	int intPropValue = uobject->property(intPropName);
	EXPECT_EQ(intPropValue, testIntValue);
	intPropValue = uobject->property(intPropName);
	EXPECT_EQ(intPropValue, testIntValue);

	std::vector<int> intVec = { 345, 32, 1322 };
	uobject->setProperty(vecPropName, intVec);
	std::vector<int> vecPropValue = uobject->property(vecPropName);
	EXPECT_EQ(vecPropValue, intVec);

	int intPropValue2 = uobject->property(intPropName);
	EXPECT_EQ(intPropValue2, testIntValue);

	const int testOverrideIntValue = 998743;
	uobject->setProperty(intPropName, testOverrideIntValue);
	int overrideIntPropValue = uobject->property(intPropName);
	EXPECT_EQ(overrideIntPropValue, testOverrideIntValue);

	const std::string unknownPropName = "Unknown property name";
	EXPECT_THROW({ uobject->property(unknownPropName); }, std::invalid_argument);
}

class MockUObject : public IUObject
{
public:
	MOCK_METHOD(
		void,
		setProperty,
		(const std::string& name, const Any&), 
		(override)
	);
	MOCK_METHOD((Any), property, (const std::string& name), (const, override));
};

TEST(UObjectTest, SetupMovable)
{
	MockUObject uobject;
	std::vector<int> testPos = { 123, 431 };
	std::vector<int> testVelocity = { 13, 41 };
	EXPECT_CALL(uobject, setProperty(property_names::position, Any(testPos))).Times(1);
	EXPECT_CALL(uobject, setProperty(property_names::velocity, Any(testVelocity)))
		.Times(1);

	setupMovable(&uobject, testPos, testVelocity);
}

TEST(UObjectTest, SetupRotatable)
{
	MockUObject uobject;
	const int testDirection = 42;
	const int testMaxDirection = 190;
	const int testAngularVel = 1;
	EXPECT_CALL(uobject, setProperty(property_names::direction, Any(testDirection)))
		.Times(1);
	EXPECT_CALL(uobject, setProperty(property_names::maxDirection, Any(testMaxDirection)))
		.Times(1);
	EXPECT_CALL(uobject, setProperty(property_names::angularVelocity, Any(testAngularVel)))
		.Times(1);

	setupRotatable(&uobject, testDirection, testMaxDirection, testAngularVel);
}