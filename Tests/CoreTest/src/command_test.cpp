#include <commands/move.h>
#include <commands/rotate.h>

#include <property_names.h>
#include <property_setters.h>
#include <uobjects/uobject.h>

#include <uobject_adapters/movable.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <vector>
#include <stdexcept>

using namespace tank_game;
using namespace ::testing;

class MockMovable : public IMovable
{
public:
	MOCK_METHOD(std::vector<int>, position, (), (const, override));
	MOCK_METHOD(void, setPosition, (const std::vector<int>& pos), (override));
	MOCK_METHOD(std::vector<int>, velocity, (), (const, override));
};

TEST(CommandTest, Move)
{
	auto movable = std::make_shared<MockMovable>();
	std::vector<int> startPos = { 12, 5 };
	std::vector<int> velocity = { -7, 3 };
	std::vector<int> destinationPos = { 5, 8 };
	EXPECT_CALL(*movable, position()).Times(AtLeast(1)).WillRepeatedly(Return(startPos));
	EXPECT_CALL(*movable, velocity()).Times(AtLeast(1)).WillRepeatedly(Return(velocity));
	EXPECT_CALL(*movable, setPosition(destinationPos)).Times(1);

	commands::Move command(movable);
	EXPECT_NO_THROW(command.execute());
}

TEST(CommandTest, MoveWithoutPosition)
{
	auto movable = std::make_shared<MockMovable>();
	std::vector<int> velocity = { 13, 41 };
	EXPECT_CALL(*movable, velocity).WillRepeatedly(Return(velocity));
	EXPECT_CALL(*movable, position()).Times(1).WillOnce(
		Throw(std::runtime_error("Can't read position"))
	);
	EXPECT_CALL(*movable, setPosition(_)).Times(0);

	commands::Move command(movable);
	EXPECT_ANY_THROW(command.execute());
}

TEST(CommandTest, MoveWithoutVelocity)
{
	auto movable = std::make_shared<MockMovable>();
	std::vector<int> position = { 13, 41 };
	EXPECT_CALL(*movable, velocity).Times(1).WillOnce(
		Throw(std::runtime_error("Can't read velocity"))
	);
	EXPECT_CALL(*movable, position()).WillRepeatedly(Return(position));
	EXPECT_CALL(*movable, setPosition(_)).Times(0);

	commands::Move command(movable);
	EXPECT_ANY_THROW(command.execute());
}

TEST(CommandTest, MoveUnmovable)
{
	auto movable = std::make_shared<MockMovable>();
	std::vector<int> position = { 13, 41 };
	std::vector<int> velocity = { 1, 2 };
	EXPECT_CALL(*movable, velocity).Times(AtLeast(1)).WillRepeatedly(Return(velocity));
	EXPECT_CALL(*movable, position()).Times(AtLeast(1)).WillRepeatedly(Return(position));
	EXPECT_CALL(*movable, setPosition(_)).Times(1).WillOnce(
		Throw(std::runtime_error("Can't change position"))
	);

	commands::Move command(movable);
	EXPECT_ANY_THROW(command.execute());
}

class MockRotatable : public IRotatable
{
public:
	MOCK_METHOD(int, direction, (), (const, override));
	MOCK_METHOD(void, setDirection, (int), (override));
	MOCK_METHOD(int, angularVelocity, (), (const, override));
	MOCK_METHOD(int, maxDirections, (), (const, override));
};

TEST(CommandTest, Rotate)
{
	auto rotatable = std::make_shared<MockRotatable>();
	EXPECT_CALL(*rotatable, direction()).Times(AtLeast(1)).WillRepeatedly(Return(20));
	EXPECT_CALL(*rotatable, angularVelocity())
		.Times(AtLeast(1)).WillRepeatedly(Return(50));
	EXPECT_CALL(*rotatable, maxDirections()).Times(AtLeast(1)).WillRepeatedly(Return(65));
	EXPECT_CALL(*rotatable, setDirection(5)).Times(1);

	commands::Rotate rotate(rotatable);
	EXPECT_NO_THROW(rotate.execute());
}